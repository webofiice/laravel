<?php

namespace Laravel\AuthSocial\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Laravel\AuthSocial\AuthSocialManager
 */
class LaServici extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Laravel\AuthSocial\Contracts\Factory';
    }
}
