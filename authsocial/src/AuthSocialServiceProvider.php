<?php

namespace Laravel\AuthSocial;

use Illuminate\Support\ServiceProvider;

class AuthSocialServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('Laravel\AuthSocial\Contracts\Factory', function ($app) {
            return new AuthSocialManager($app);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['Laravel\AuthSocial\Contracts\Factory'];
    }
}
