<?php

namespace Laravel\AuthSocial\Contracts;

interface Factory
{
    /**
     * Get an OAuth provider implementation.
     *
     * @param  string  $driver
     * @return \Laravel\AuthSocial\Contracts\Provider
     */
    public function driver($driver = null);
}
